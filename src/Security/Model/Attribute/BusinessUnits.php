<?php

namespace Ds\Component\Security\Model\Attribute;

/**
 * Trait BusinessUnits
 *
 * @package Ds\Component\Security
 */
trait BusinessUnits
{
    use Accessor\BusinessUnits;

    /**
     * @var array
     */
    private $businessUnits;
}
