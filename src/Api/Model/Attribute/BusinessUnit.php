<?php

namespace Ds\Component\Api\Model\Attribute;

/**
 * Trait BusinessUnit
 *
 * @package Ds\Component\Api
 */
trait BusinessUnit
{
    use Accessor\BusinessUnit;

    /**
     * @var \Ds\Component\Api\Model\BusinessUnit
     */
    private $businessUnit;
}
